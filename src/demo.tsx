import React from 'react';
import MaterialTable, { Column } from 'material-table';
import { deflateSync } from 'zlib';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
        width: 800,
        display: 'flex',
        },
  }),
);
interface Row {
  chemotherapeutic: string;
  dose: number;
  startDay: number;
  endDay: number;
  frequency: string;
}

interface TableState {
  columns: Array<Column<Row>>;
  data: Row[];
}

export default function MaterialTableDemo() {
  const classes = useStyles();
  const [state, setState] = React.useState<TableState>({
    columns: [
      { title: 'Chemotherapeutic', field: 'chemotherapeutic' },
      { title: 'Dose (mg/m^2)', field: 'dose' },
      { title: 'Start Day', field: 'startDay', type: 'numeric' },
      { title: 'End Day', field: 'endDay', type: 'numeric' },
      { title: 'Frequency', field: 'frequency'}
    ],
    data: [
      { chemotherapeutic: 'Test1', dose: 60, startDay:0,endDay: 56, frequency: '14 days'},
      { chemotherapeutic: 'Test2', dose: 600, startDay:0,endDay: 56, frequency: '14 days'},
      { chemotherapeutic: 'Test3', dose: 175, startDay:56,endDay: 84, frequency: '14 days'},

    ],
  });

  return (
    <div className={classes.root}>
    <MaterialTable
      title="Build a Regimen"
      columns={state.columns}
      data={state.data}
      options={{
        selection: true
      }}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState(prevState => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }}
    />
    </div>
  );
}