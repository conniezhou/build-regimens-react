## Instruction
Use npm start to start and the index page would show the usage of 'material-table' package

## Import
`import MaterialTable, { Column } from 'material-table';`

## Example
![Example](example.png)